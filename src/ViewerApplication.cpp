#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/cameras.hpp"
#include "utils/gltf.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>

void keyCallback(
    GLFWwindow *window, int key, int scancode, int action, int mods)
{
  if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
    glfwSetWindowShouldClose(window, 1);
  }
}

int ViewerApplication::run()
{

  // Loading the glTF file
  tinygltf::Model model;
  if (!loadGltfFile(model)) {return -1;};

  glm::vec3 bboxMin, bboxMax;
  computeSceneBounds(model, bboxMin, bboxMax);
  glm::vec3 center = (bboxMax + bboxMin) / 2.0f;
  glm::vec3 diag = bboxMax - bboxMin;

  // Loader shaders
  const auto glslProgram = compileProgram({m_ShadersRootPath / m_vertexShader,
      m_ShadersRootPath / m_fragmentShader});

  const auto modelViewProjMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
  const auto modelViewMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
  const auto normalMatrixLocation =
      glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

  const auto lightingDirectionLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightDirection");
  const auto lightingIntensityLocation =
      glGetUniformLocation(glslProgram.glId(), "uLightIntensity");

  const auto baseColorTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");

  const auto baseColorFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

  const auto metallicRoughnessLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");

  const auto metallicFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");

  const auto rougnessFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uRougnessFactor");

  const auto emissiveTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");

  const auto emissiveFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");

   const auto occlusionTextureLocation =
      glGetUniformLocation(glslProgram.glId(), "uOccusionTexture");

   const auto occlusionFactorLocation =
      glGetUniformLocation(glslProgram.glId(), "uOcclusionFactor");

   const auto activeOcclusionLocation =
        glGetUniformLocation(glslProgram.glId(), "uActiveOcclusion");

  glm::vec3 lightDirection(1, 1, 1);
  glm::vec3 lightIntensity(1, 1, 1);
  bool lightOnCamera = false;
  bool activeOcclusion = true;

  // Build projection matrix
  auto maxDistance = glm::length(diag); //  use scene bounds 
  maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
  float camSpeed = 0.4f * maxDistance;

  const auto projMatrix =
      glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
         0.001f * maxDistance, 1.5f * maxDistance);


  std::unique_ptr<CameraController> cameraController =
      std::make_unique<TrackballCameraController>(
          m_GLFWHandle.window(), camSpeed);
  if (m_hasUserCamera) {
    cameraController->setCamera(m_userCamera);
  } else {
    const glm::vec3 eye = (bboxMax.z != bboxMin.z) ? center + diag : center + 2.f * glm::cross(diag, glm::vec3(0, 1, 0));
    // Use scene bounds to compute a better default camera
    cameraController->setCamera(Camera{eye, center, glm::vec3(0, 1, 0)});
  }

  // Creation of Texture
  const std::vector<GLuint> textureObjects = createTextureObjects(model);

  GLuint whiteTexture;
  float white[] = {1, 1, 1, 1};

  glActiveTexture(GL_TEXTURE0);
  glGenTextures(1, &whiteTexture);
  glBindTexture(GL_TEXTURE_2D, whiteTexture);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA,
      GL_FLOAT, white);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
  glBindTexture(GL_TEXTURE_2D, 0);


  // Creation of Buffer Objects
  const std::vector<GLuint> bufferObjects = createBufferObjects(model);

  // Creation of Vertex Array Objects
  std::vector<VaoRange> meshIndexToVaoRange;
  const auto vertexArrayObjects =
      createVertexArrayObjects(model, bufferObjects, meshIndexToVaoRange);

  // Setup OpenGL state for rendering
  glEnable(GL_DEPTH_TEST);
  glslProgram.use();

  // Lambda function to draw the scene
  const auto drawScene = [&](const Camera &camera) {
    glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const glm::mat4 viewMatrix = camera.getViewMatrix();

    if (lightingDirectionLocation >= 0) {
      if (lightOnCamera) {
        glUniform3f(lightingDirectionLocation, 0, 0, 1);
      } else {
        glm::vec3 lighfFinalDirection = glm::vec3(
            glm::normalize(viewMatrix * glm::vec4(lightDirection, 0)));
        glUniform3f(lightingDirectionLocation, lighfFinalDirection.x,
            lighfFinalDirection.y, lighfFinalDirection.z);
      }
    }

    if (lightingIntensityLocation >= 0) {
      glUniform3f(lightingIntensityLocation, lightIntensity.x, lightIntensity.y,
          lightIntensity.z);
    }

    
    const auto bindMaterial = [&](const auto materialIndex) {
      if (baseColorTextureLocation >= 0) {
            GLuint textureObject = whiteTexture;
            GLuint metallicRoughnessTexture = 0;
            GLuint emissiveTexture = 0;
            GLuint occlusionTexture = 0;
            glm::vec4 colorFactor(1);
            float metallicFactor = 1.0f;
            float roughnessFactor = 1.0f;
            float occlusionStrength = 1.0f;
            glm::vec3 emissiveFactor(1);
            if (materialIndex >= 0) {
              const auto &material = model.materials[materialIndex];
              const auto &pbrMetallicRoughness = material.pbrMetallicRoughness;
              if (pbrMetallicRoughness.baseColorTexture.index >= 0) {

                const auto &texture =
                    model.textures[pbrMetallicRoughness.baseColorTexture.index];
                if (texture.source >= 0) {
                  textureObject = textureObjects[texture.source];
                  colorFactor =
                      glm::vec4((float)pbrMetallicRoughness.baseColorFactor[0],
                          (float)pbrMetallicRoughness.baseColorFactor[1],
                          (float)pbrMetallicRoughness.baseColorFactor[2],
                          (float)pbrMetallicRoughness.baseColorFactor[3]);
                }
              }

              if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
                const auto &texture = model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
                if (texture.source >= 0) {
                  metallicRoughnessTexture = textureObjects[texture.source];
                }
                metallicFactor = (float)pbrMetallicRoughness.metallicFactor;
                roughnessFactor = (float)pbrMetallicRoughness.roughnessFactor;
              }

              if (material.emissiveTexture.index >= 0) {
                const auto &texture =
                    model.textures[material.emissiveTexture.index];
                if (texture.source >= 0) {
                  emissiveTexture = textureObjects[texture.source];
                }

                emissiveFactor = glm::vec3((float)material.emissiveFactor[0],
                  (float)material.emissiveFactor[1],
                  (float)material.emissiveFactor[2]);
              }

              if (material.occlusionTexture.index >= 0) {
                const auto &texture =
                    model.textures[material.occlusionTexture.index];
                if (texture.source >= 0) {
                  occlusionTexture = textureObjects[texture.source];
                }

                occlusionStrength = (float)material.occlusionTexture.strength;
              }

            }

            if (baseColorTextureLocation >= 0) {
              glActiveTexture(GL_TEXTURE0);
              glBindTexture(GL_TEXTURE_2D, textureObject);
              glUniform1i(baseColorTextureLocation, 0);
            }

            if (metallicRoughnessLocation >= 0) {
              glActiveTexture(GL_TEXTURE1);
              glBindTexture(GL_TEXTURE_2D, metallicRoughnessTexture);
              glUniform1i(metallicRoughnessLocation, 1);
            }

            if (emissiveTextureLocation >= 0) {
              glActiveTexture(GL_TEXTURE2);
              glBindTexture(GL_TEXTURE_2D, emissiveTexture);
              glUniform1i(emissiveTextureLocation, 2);
            }

            if (occlusionTextureLocation >= 0) {
              glActiveTexture(GL_TEXTURE3);
              glBindTexture(GL_TEXTURE_2D, occlusionTexture);
              glUniform1i(occlusionTextureLocation, 3);
            }


            if (baseColorFactorLocation >= 0) {
              glUniform4f(baseColorFactorLocation, colorFactor.r, colorFactor.g,
                  colorFactor.b, colorFactor.a);
            }

            if (metallicFactorLocation >= 0) {
              glUniform1f(metallicFactorLocation, metallicFactor);
            }
          
            if (rougnessFactorLocation >= 0) {
              glUniform1f(rougnessFactorLocation, roughnessFactor);
            }

            if (emissiveFactorLocation >= 0) {
              glUniform3f(emissiveFactorLocation, emissiveFactor.x,
                  emissiveFactor.y, emissiveFactor.z);
            }

            if (occlusionFactorLocation >= 0) {
              glUniform1f(occlusionFactorLocation, occlusionStrength);
            }

            if (activeOcclusionLocation >= 0) {
              glUniform1i(activeOcclusionLocation, activeOcclusion);
            }
        }
    };

    const std::function<void(int, const glm::mat4 &)> drawNode =
        [&](int nodeIdx, const glm::mat4 &parentMatrix) {
          const tinygltf::Node& node = model.nodes[nodeIdx];
          glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
          
          if (node.mesh >= 0) {
            
            glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
            glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
            glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

            glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewProjectionMatrix));
            glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
            glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));
          
            
            const VaoRange& vaoRange = meshIndexToVaoRange[node.mesh];
            
            for (size_t primitiveIdx = 0; primitiveIdx < model.meshes[node.mesh].primitives.size(); ++primitiveIdx) {
             
              const tinygltf::Primitive& primitive =
                  model.meshes[node.mesh].primitives[primitiveIdx];

              bindMaterial(primitive.material);
              glBindVertexArray(vertexArrayObjects[vaoRange.begin + primitiveIdx]);
              if (primitive.indices >= 0) {

 
                const tinygltf::Accessor& accessor =
                    model.accessors[primitive.indices];
                const int byteOffset =
                    accessor.byteOffset +
                    model.bufferViews[accessor.bufferView].byteOffset;

                glDrawElements(primitive.mode, GLsizei(accessor.count), accessor.componentType, (const GLvoid *)byteOffset); 
              } else {
                const int accessorIdx = (*begin(primitive.attributes)).second;
                const tinygltf::Accessor& accessor = model.accessors[accessorIdx];
                glDrawArrays(primitive.mode, 0, GLsizei(accessor.count));
              }
            }
          }
          for (const int childNodeIdx : node.children) {
            drawNode(childNodeIdx, modelMatrix);
          }
        };

    // Draw the scene referenced by gltf file
    if (model.defaultScene >= 0) {
      for (int nodeIdx : model.scenes[model.defaultScene].nodes) {
        drawNode(nodeIdx, glm::mat4(1));
      }
    }
  };

  if (m_OutputPath != "") {
    const size_t numComponents = 3;
    std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * numComponents);

    renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), [&]() { drawScene(cameraController->getCamera()); });
    flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());

      const auto strPath = m_OutputPath.string();
      stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3,
          pixels.data(), 0);

      return 0;
  }


  // Loop until the user closes the window
  for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
       ++iterationCount) {
    const auto seconds = glfwGetTime();

    const auto camera = cameraController->getCamera();
    drawScene(camera);

    // GUI code:
    imguiNewFrame();

    {
      ImGui::Begin("GUI");
      ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
          1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
      if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
        ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
            camera.eye().z);
        ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
            camera.center().y, camera.center().z);
        ImGui::Text(
            "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

        ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
            camera.front().z);
        ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
            camera.left().z);

        if (ImGui::Button("CLI camera args to clipboard")) {
          std::stringstream ss;
          ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
             << camera.eye().z << "," << camera.center().x << ","
             << camera.center().y << "," << camera.center().z << ","
             << camera.up().x << "," << camera.up().y << "," << camera.up().z;
          const auto str = ss.str();
          glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
        }

        static int camType = 0;
        const auto cameraControllerTypeChanged =
            ImGui::RadioButton("Trackball", &camType, 0) ||
            ImGui::RadioButton("First Person", &camType, 1);

        if (cameraControllerTypeChanged) {
          const Camera currentCam = cameraController->getCamera();
          if (camType == 0) {
            cameraController = std::make_unique<TrackballCameraController>(
                m_GLFWHandle.window(), camSpeed);
          } else if (camType == 1) {
            cameraController = std::make_unique<FirstPersonCameraController>(
                m_GLFWHandle.window(), camSpeed);
          }
          cameraController->setCamera(currentCam);
        }

        if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {

          static float theta = 0.0f;
          static float phi = 0.0f;

          if (ImGui::SliderFloat("Theta", &theta, 0.0f, glm::pi<float>()) || ImGui::SliderFloat("Phi", &phi, 0.0f, glm::pi<float>() * 2)) {
            lightDirection = glm::vec3(glm::sin(theta) * glm::cos(phi),
                glm::cos(theta), glm::sin(theta) * glm::sin(phi));
          }

          static glm::vec3 color(1.f, 1.f, 1.f);
          static float intensityFactor = 1.f;


          if (ImGui::ColorEdit3("Color", (float *)&color) ||
              ImGui::InputFloat("Intensity", &intensityFactor)) {
                lightIntensity = color * intensityFactor;
          }

          ImGui::Checkbox("Light on camera", &lightOnCamera);
          ImGui::Checkbox("Occlusion Activation", &activeOcclusion);
        }
      }
      ImGui::End();
    }

    imguiRenderFrame();

    glfwPollEvents(); // Poll for and process events

    auto ellapsedTime = glfwGetTime() - seconds;
    auto guiHasFocus =
        ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
    if (!guiHasFocus) {
      cameraController->update(float(ellapsedTime));
    }

    m_GLFWHandle.swapBuffers(); // Swap front and back buffers
  }

  // TODO clean up allocated GL data

  return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
    uint32_t height, const fs::path &gltfFile,
    const std::vector<float> &lookatArgs, const std::string &vertexShader,
    const std::string &fragmentShader, const fs::path &output) :
    m_nWindowWidth(width),
    m_nWindowHeight(height),
    m_AppPath{appPath},
    m_AppName{m_AppPath.stem().string()},
    m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
    m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
    m_gltfFilePath{gltfFile},
    m_OutputPath{output}
{
  if (!lookatArgs.empty()) {
    m_hasUserCamera = true;
    m_userCamera =
        Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
            glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
            glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
  }

  if (!vertexShader.empty()) {
    m_vertexShader = vertexShader;
  }

  if (!fragmentShader.empty()) {
    m_fragmentShader = fragmentShader;
  }

  ImGui::GetIO().IniFilename =
      m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
                                  // positions in this file

  glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

  printGLVersion();
}


bool ViewerApplication::loadGltfFile(tinygltf::Model &model) {

    tinygltf::TinyGLTF loader;
    std::string err, warn;

    bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, m_gltfFilePath.string());

    if (!warn.empty()) {
      printf("Warn: %s\n", warn.c_str());
    }

    if (!err.empty()) {
      printf("Err: %s\n", err.c_str());
    }

    if (!ret) {
      printf("Failed to parse glTF\n");
    }

    return ret;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(const tinygltf::Model &model)
{
  std::vector<GLuint> bufferObjects(model.buffers.size(), 0); 
  glGenBuffers(model.buffers.size(), bufferObjects.data());

  for (size_t i = 0; i < model.buffers.size(); ++i) {
    glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);
    glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
  }

  glBindBuffer(GL_ARRAY_BUFFER, 0);
  return std::move(bufferObjects);
}


std::vector<GLuint> ViewerApplication::createVertexArrayObjects(
    const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
    std::vector<VaoRange> &meshIndexToVaoRange)
{
    const GLuint VERTEX_ATTRIB_POSITION_IDX = 0;
    const GLuint VERTEX_ATTRIB_NORMAL_IDX = 1;
    const GLuint VERTEX_ATTRIB_TEXCOORD0_IDX = 2;

    std::vector<GLuint> vertexArrayObjects;
    
     meshIndexToVaoRange.resize(model.meshes.size());

      for (size_t meshIdx = 0; meshIdx < model.meshes.size(); ++meshIdx) {

       const auto vaoOffset = GLsizei(vertexArrayObjects.size());
       const auto vaoCount = GLsizei(model.meshes[meshIdx].primitives.size());

       vertexArrayObjects.resize(vertexArrayObjects.size() + model.meshes[meshIdx].primitives.size());

       meshIndexToVaoRange.push_back(VaoRange{vaoOffset, vaoCount});

       glGenVertexArrays(vaoCount,
           &vertexArrayObjects[vaoOffset]);

       for (size_t primitiveIdx = 0;
            primitiveIdx < model.meshes[meshIdx].primitives.size();
            ++primitiveIdx) {

         glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);

         const tinygltf::Primitive &primitive =
             model.meshes[meshIdx].primitives[primitiveIdx];

         const size_t nbAttribute = 3;
         std::string attributes[nbAttribute] = {
             "POSITION", "NORMAL", "TEXCOORD_0"};
         const GLuint attributesType[nbAttribute] = {
             VERTEX_ATTRIB_POSITION_IDX, VERTEX_ATTRIB_NORMAL_IDX, VERTEX_ATTRIB_TEXCOORD0_IDX};
         for (size_t attributeIdx = 0; attributeIdx < nbAttribute;
              attributeIdx++) {
           const auto iterator =
               primitive.attributes.find(attributes[attributeIdx]);

           if (iterator != end(primitive.attributes)) {
             const auto accessorIdx = (*iterator).second;
             const auto &accessor = model.accessors[accessorIdx];
             const auto &bufferView = model.bufferViews[accessor.bufferView];
             const auto bufferIdx = bufferView.buffer;

             const auto bufferObject = bufferObjects[bufferIdx];

             glEnableVertexAttribArray(attributesType[attributeIdx]);
             glBindBuffer(GL_ARRAY_BUFFER, bufferObject);

             const auto byteOffset =
                 accessor.byteOffset + bufferView.byteOffset;

             glVertexAttribPointer(attributesType[attributeIdx], accessor.type,
                 accessor.componentType, GL_FALSE, GLsizei(bufferView.byteStride),
                 (const GLvoid *)byteOffset);
           }
         }

         if (primitive.indices >= 0) {
           const auto accessorIdx = primitive.indices;
           const auto &accessor = model.accessors[accessorIdx];
           const auto &bufferView = model.bufferViews[accessor.bufferView];
           const auto bufferIdx = bufferView.buffer;

           glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
         }
       }
     }
     
      glBindVertexArray(0);

     return vertexArrayObjects;
}


std::vector<GLuint> ViewerApplication::createTextureObjects(
    const tinygltf::Model &model) const
{

  tinygltf::Sampler defaultSampler;
  defaultSampler.minFilter = GL_LINEAR;
  defaultSampler.magFilter = GL_LINEAR;
  defaultSampler.wrapS = GL_REPEAT;
  defaultSampler.wrapT = GL_REPEAT;
  defaultSampler.wrapR = GL_REPEAT;

  std::vector<GLuint> textureObjects(model.textures.size(), 0);
  glActiveTexture(GL_TEXTURE0);
  glGenTextures(GLsizei(model.textures.size()), textureObjects.data());
  for (size_t textIdx = 0; textIdx < model.textures.size(); textIdx++) {
    const tinygltf::Texture &textureInfo = model.textures[textIdx];
    glBindTexture(GL_TEXTURE_2D, textureObjects[textIdx]);

    assert(textureInfo.source >= 0); // ensure a source image is present
    const tinygltf::Image& image = model.images[textureInfo.source];

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0, GL_RGBA, image.pixel_type, image.image.data());

    const tinygltf::Sampler &sampler = textureInfo.sampler >= 0 ? model.samplers[textureInfo.sampler] : defaultSampler;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
        sampler.minFilter != -1 ? sampler.minFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
        sampler.magFilter != -1 ? sampler.magFilter : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, sampler.wrapS);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, sampler.wrapT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, sampler.wrapR);

    if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
        sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
        sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
        sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
      glGenerateMipmap(GL_TEXTURE_2D);
    }

  }
  glBindTexture(GL_TEXTURE_2D, 0);

  return std::move(textureObjects);
}