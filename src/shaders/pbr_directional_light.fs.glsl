#version 330

in vec3 vViewSpaceNormal;
in vec3 vViewSpacePosition;
in vec2 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

uniform sampler2D uBaseColorTexture;
uniform sampler2D uMetallicRoughnessTexture;
uniform sampler2D uEmissiveTexture;
uniform sampler2D uOccusionTexture;

uniform vec4 uBaseColorFactor;
uniform float uMetallicFactor;
uniform float uRougnessFactor;
uniform float uOcclusionFactor;
uniform vec3 uEmissiveFactor;

uniform int uActiveOcclusion;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

// We need some simple tone mapping functions
// Basic gamma = 2.2 implementation
// Stolen here:
// https://github.com/KhronosGroup/glTF-Sample-Viewer/blob/master/src/shaders/tonemapping.glsl

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec4 SRGBtoLINEAR(vec4 srgbIn)
{
  return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w);
}

void main()
{
  vec3 N = normalize(vViewSpaceNormal);
  vec3 L = uLightDirection;

  vec4 baseColorFromTexture =
      SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
  float NdotL = clamp(dot(N, L), 0., 1.);

  vec4 baseColor = baseColorFromTexture * uBaseColorFactor;

  vec4 metallicRoughnessFromTexture =
      texture(uMetallicRoughnessTexture, vTexCoords);

  vec3 metallic = vec3(metallicRoughnessFromTexture.b * uMetallicFactor);
  float roughness = (metallicRoughnessFromTexture.g * uRougnessFactor);


  vec3 dielectricSpecular = vec3(0.04);
  vec3 black = vec3(0);

     
  vec3 V = normalize(-vViewSpacePosition);
  vec3 H = normalize(L + V);
  float VdotN = clamp(dot(V, N), 0., 1.);
  float LdotN = clamp(dot(L, N), 0., 1.);
  float VdotH = clamp(dot(V, H), 0., 1.);
  float NdotH = clamp(dot(N, H), 0., 1.);

  vec3 c_diff = mix(baseColor.rgb * (1 - dielectricSpecular), black, metallic);
  vec3 f0 = mix(dielectricSpecular, baseColor.rgb, metallic);
  
  float alpha = roughness * roughness;
  float sqrAlpha = alpha * alpha;

  float baseShlickFactor = 1 - abs(VdotH);
  float shlickFactor = baseShlickFactor * baseShlickFactor; // power 2
  shlickFactor *= shlickFactor;                             // power 4
  shlickFactor *= baseShlickFactor;                         // power 5
  vec3 F = f0 + (vec3(1) - f0) * shlickFactor;

  float visDenom = 
      LdotN * sqrt(VdotN * VdotN * (1 - sqrAlpha) + sqrAlpha) +
      VdotN * sqrt(NdotL * LdotN * (1 - sqrAlpha) + sqrAlpha);

  float vis = 0;
  if (visDenom > 0.) {
    vis = 0.5f / visDenom;
  }

  float baseDenomD = (NdotH * NdotH * (sqrAlpha - 1.) + 1.);
  float D = M_1_PI * sqrAlpha / (baseDenomD * baseDenomD);

  vec3 f_specular = F * vis * D;
  vec3 f_diffuse = (1. - F) * c_diff * M_1_PI;

  vec3 emissive = SRGBtoLINEAR(texture2D(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;


  vec3 color = (f_diffuse + f_specular) * uLightIntensity * NdotL + emissive;

  if (uActiveOcclusion == 1) {
    float occlusion = texture2D(uOccusionTexture, vTexCoords).r;
    color = mix(color, color * occlusion, uOcclusionFactor);
  }

  fColor = LINEARtoSRGB(color);
}